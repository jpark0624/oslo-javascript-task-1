// Button elements
const elLoanBtn = document.getElementById('btn-loan');
const elTransferToBankBtn = document.getElementById('btn-bank')
const elWorkBtn = document.getElementById('btn-work');
const elBuyBtn = document.getElementById('btn-buy');

// Account elements
const elBalance = document.getElementById('balance');
const elPay = document.getElementById('pay');

// Laptop elements
const elLaptopPick = document.getElementById('laptopPick');
const elLaptopFeatures = document.getElementById('laptopFeatures');
const elLaptopImage = document.getElementById('laptopImage')
const elLaptopModelName = document.getElementById('laptopModelName');
const elLaptopDescription = document.getElementById('laptopDescription');
const elLaptopPrice = document.getElementById('laptopPrice');

let balance = 5000;
let pay = 0;
let loan = false;

// Get a loan function
const getLoan = function() {
    let amount = prompt("How much loan do you need?");
    if(amount != null) {
        amount = parseInt(amount);
        if(amount > balance * 2) {
            window.alert("Sorry, you cannot get a loan more than double of your bank balance.");
        } else if (loan){
            window.alert("You cannot get more than one bank loan from us.");
        } else if (amount <= balance * 2 && !loan) {
            balance += amount;
            loan = true;
        }
    }
}

// Updating balance
const balanceAdded = function() {
    elBalance.innerText = balance;
    elPay.innerText = pay;
    showLaptop();
}

elLoanBtn.addEventListener('click', function(loan) {
    getLoan();
    balanceAdded();
})

// Earning 100kr per click (That would be a dream job)
const workMoney = function() {
    pay += 100;
}

elWorkBtn.addEventListener('click', function(work) {
    workMoney();
    balanceAdded();
})

const transferPayrollToAccount = function() {
    if(pay > 0) {
        balance += pay;
        pay = 0;
    }
}

elTransferToBankBtn.addEventListener('click', function(bank) {
    transferPayrollToAccount();
    balanceAdded();
})

// Laptop info.
const laptopInfo = [
    { 
        name: "Razer Blade",
        price: 5490, 
        id: "razer",
        description: "A brand new Razer laptop that is as sharp as a blade. You might be able to cut Apples with it.",
        image: "https://static.techspot.com/images/products/2019/laptops/org/2019-02-20-product-2.png"},
    { 
        name: "Alienware Area",
        price: 5900, 
        id: "alienware",
        description: "A powerful gaming laptop which will lead your gaming experice so much higher. Yes, so high up that you might enter into an UFO, AREA where there are lots of ALIEN. Buy now ALIENware AREA.",
        image: "https://techhubtoday.com/wp-content/uploads/2019/05/Alienware-m17_display-tobii_white-678_678x452.png"},
    { 
        name: "MSI Stealth",
        price: 5770,
        id: "msi",
        description: "MSI secretly stands for 'Muliple Streams of Income'. If you buy this laptop, you will dive into a deep powerful money flow.",
        image: "https://www.digitweek.com/wp-content/uploads/2020/07/MSI-GS65-Stealth-483-Review.png"},
    { 
        name: "Asus Zephyrus",
        price: 5890,
        id: "asus",
        description: "A fancy schmancy laptop with dual screen. Do you want to impress girls in class? Bring Zephyrus to your class, open up the laptop and make sure it's on the max brightness.", 
        image: "https://cdn.vox-cdn.com/thumbor/jVWkGuzD572eBI85RgJM-ue-8t8=/0x0:5000x5000/1400x933/filters:focal(2100x2100:2900x2900):no_upscale()/cdn.vox-cdn.com/uploads/chorus_image/image/66591602/GX550_C04_v6_Light.0.png"}
];

elLaptopPick.addEventListener('click', function() {
    balanceAdded();
})

let chosenLaptop = elLaptopPick;

function showLaptop() {
    let laptop = getLaptop();
    elLaptopModelName.innerText = laptop.name;
    elLaptopPrice.innerText = laptop.price;
    elLaptopImage.src = laptop.image;
    elLaptopDescription.innerText = laptop.description;
    elLaptopFeatures.innerText = laptop.description;
}

function getLaptop() {
    for(let i = 0; i < laptopInfo.length; i++) {
        if(chosenLaptop.value === laptopInfo[i].id) {
            laptop = laptopInfo[i];
            return laptop;
        }
    }
}

showLaptop();

function buyCheck() {
    if(balance >= laptop.price) {
        return true;
    }
    return false;
}

function purchaseLaptop() {
    if(buyCheck()) {
        balance -= laptop.price;
        window.alert(`Your new balance is now ${balance}kr`);
        loan = false;
    } else {
        window.alert("You do not have enough money to purchase the laptop.");
    }
}

elBuyBtn.addEventListener('click', function(){
    purchaseLaptop();
    balanceAdded();
})

